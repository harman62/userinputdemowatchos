//
//  InterfaceController.swift
//  UserInputDemo WatchKit Extension
//
//  Created by Harman Kaur on 2019-10-16.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    //MARK: Outlets
    //---------------
    
    @IBOutlet weak var responseLabel: WKInterfaceLabel!
    
    //MARK: Default Functions
    //-----------------
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    //MARK: Custom functions & Actions
    //-----------------------

    @IBAction func replyButtonPressed() {
        print("Reply button pressed")
        let cannedResponses = ["8:00am", "9:00am", "10:00am", "11:00am"]
        presentTextInputController(withSuggestions: cannedResponses, allowedInputMode: .plain) {
            
            (results) in
            
            
            if (results != nil && results!.count > 0) {
                let userResponse = results?.first as? String
                self.responseLabel.setText(userResponse)
            }
        }
    }
}
